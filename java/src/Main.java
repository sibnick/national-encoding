import java.io.*;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.*;
import java.util.*;

public class Main {

    public static void main(String args[])
            throws Exception {

        // 0 - class name
        // 1 - file name
        String classTmpl = "enum {0} = loadOneByteCodepage(import(\"{1}\"));\n";

        SortedMap<String, Charset> allCharsets = java.nio.charset.Charset.availableCharsets();
        StringBuilder classes = new StringBuilder();
        StringBuilder dict = new StringBuilder();

        byte data[] = new byte[256];
        for(int i=0; i<data.length; i++){
            data[i] = (byte)i;
        }

        for (Map.Entry<String, Charset> e : allCharsets.entrySet()) {
            String name = e.getKey();
            Charset charset = e.getValue();
            try {
                float maxBytes = charset.newEncoder().maxBytesPerChar();
                if (maxBytes > 1.0f) {
                    System.out.println("skip: " + name + "/" + maxBytes);
                    continue;
                }
            } catch (java.lang.UnsupportedOperationException ex) {
                System.out.println("skip: " + name + "/UnsupportedOperationException");
                continue;
            }
            String prefix = "enc/";
            String s = new String(data, 0, 256, name);
            byte encoded[] = s.getBytes("UTF-8");

            String className = genClassName(name);
		    classes.append(java.text.MessageFormat.format(classTmpl, className, "national/codepages/"+name.toLowerCase()+"_utf8.bin"));

            FileOutputStream utf = new FileOutputStream(prefix + name.toLowerCase() + "_utf8.bin");
            utf.write(encoded);
            utf.close();
            for(String alias: charset.aliases()){
                dict.append("[\"").append(alias).append("\" : ").append(className).append("],\n");
            }

        }
        try(FileOutputStream gen = new FileOutputStream("enc/all_encoding.d")) {
            gen.write(classes.toString().getBytes());
            gen.write(dict.toString().getBytes());
        }


    }

    private static String genClassName(String name) {
        StringBuilder sb = new StringBuilder();
        boolean upper = false;
        for(int i=0; i<name.length(); i++){
            char ch = name.charAt(i);
            if(i==0){
                sb.append(Character.toUpperCase(ch));
            }else if (ch=='-'){
                upper = true;
            }else if (Character.isDigit(ch)){
                upper = true;
                sb.append(ch);
            }else{
                sb.append(upper? Character.toUpperCase(ch): ch);
                upper = false;
            }
        }
        return sb.toString();
    }
}