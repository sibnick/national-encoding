﻿
# national charsets support

## Key features

 - Rangify charset functions
 - Support more than 100 national one byte code pages

## Sample
  
    import national.encoding;
    unittest
    {
		import std.array;
		dstring s = "123Я";
		auto cp1251 = Windows1251.encode(s).array;
		assert(cp1251[$-1]==223);
		auto utf32 = Windows1251.decode(cp1251).array;
		assert(utf32[$-1]=='Я');

		ubyte[] cp866 = CHARSETS["cp866"].encode(s).array;
		assert(cp866[$-1]==159);
		utf32 = CHARSETS["cp866"].decode(cp866).array;
		assert(utf32[$-1]=='Я');
	}
