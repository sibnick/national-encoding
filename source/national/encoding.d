module national.encoding;
@safe

import std.range.primitives;
import std.algorithm;
import std.array;
import std.traits;
import std.typecons;
import std.conv:to;

auto loadOneByteCodepage(dstring codepage) pure nothrow
{
	Symbol[] encodeDict;
	foreach(idx, c; codepage)
		encodeDict ~= Symbol(c, cast(ubyte)idx);
	encodeDict.sort!"a.utfCode>b.utfCode"();
	return OneByteCodePage(encodeDict, codepage);
}

struct Symbol
{
	dchar utfCode;
	ubyte code;
}

struct OneByteCodePage{

	private {
		Symbol[] encodeDict;
		dstring decodeDict;
		char replaceBad = '?';
	}

	public{
		import std.array;
		auto decode(UbyteRange)(UbyteRange input) pure nothrow @nogc
			if (is(ElementType!UbyteRange == ubyte))
		{
			struct Range{
				UbyteRange input;
				dstring decodeDict;
				auto front()
				{
					return decodeDict[input.front];
				}
				auto empty() {return input.empty;}
				void popFront() {input.popFront;}
			}
			return Range(input, decodeDict);
		}
		
		auto encode(CharRange)(CharRange input) pure nothrow @nogc
			if (isSomeString!CharRange || isInputRange!CharRange && isSomeChar!(ElementEncodingType!CharRange))
		{
			return input.map!(x => encodeChar(x));
		}

		bool canEncode(dchar c) pure nothrow @nogc
		{
			auto range = encodeDict.find!"a.utfCode==b.utfCode"(Symbol(c, 0));
			return range.empty;
		}

		ubyte encodeChar(C)(C c) pure nothrow
			if (isSomeChar!C)
		{
			dchar dc = to!dchar(c);
			auto range = encodeDict.find!"a.utfCode==b.utfCode"(Symbol(c, 0));
			auto code = range.empty? replaceBad : range.front.code;
			assert (code<=ubyte.max);
			return cast(ubyte)(code);
		}
	}
}



